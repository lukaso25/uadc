# uadc

This repository contains Kicad EDA 6.0 PCB desing of the USB stereo audio AD converter with diferrential input using AK5522 converter and STM32F042 microcontroller.



This hardware design is licenced under CERN Open Hardware Licence Version 2 - Weakly Reciprocal (see LICENSE.txt)

Copyright Lukáš Otava, lukaso25@seznam.cz

